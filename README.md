# Cuber

Rubik's Cube using typescript for 100% client side single page application with Vue and Vuetify.

# [虚拟魔方](https://huazhechen.gitee.io/cuber)

- 极致触摸操控体验

  ![touch](https://gitee.com/huazhechen/cuber/raw/master/screenshot/touch.gif)

- 全功能键盘

  ![keyboard](https://gitee.com/huazhechen/cuber/raw/master/screenshot/keyboard.gif)

- 镜像助手

  ![mirror](https://gitee.com/huazhechen/cuber/raw/master/screenshot/mirror.gif)

- 涂色助手

  ![cf](https://gitee.com/huazhechen/cuber/raw/master/screenshot/cf.gif)

- 撤销操作

  ![backspace](https://gitee.com/huazhechen/cuber/raw/master/screenshot/backspace.gif)

- 重新打乱

  ![random](https://gitee.com/huazhechen/cuber/raw/master/screenshot/random.gif)

- 外观自定义

  ![tune](https://gitee.com/huazhechen/cuber/raw/master/screenshot/tune.gif)

# [公式宝典](https://huazhechen.gitee.io/cuber/algs.html)

- 查看与播放

  ![player](https://gitee.com/huazhechen/cuber/raw/master/screenshot/player.gif)

- 单步播放与播放控制

  ![play-control](https://gitee.com/huazhechen/cuber/raw/master/screenshot/play-control.gif)

- 公式跳转

  ![alg-choose](https://gitee.com/huazhechen/cuber/raw/master/screenshot/alg-choose.gif)

- 快捷公式跳转

  ![alg-list](https://gitee.com/huazhechen/cuber/raw/master/screenshot/alg-list.gif)

- 公式修改

  ![modify](https://gitee.com/huazhechen/cuber/raw/master/screenshot/modify.gif)

# [导演模式](https://huazhechen.gitee.io/cuber/director.html)

- 布置场景并截图

  ![snap](https://gitee.com/huazhechen/cuber/raw/master/screenshot/snap.gif)

- 动画编写/播放/导出

  ![action](https://gitee.com/huazhechen/cuber/raw/master/screenshot/action.gif)

- 自动布置场景

  ![reverse](https://gitee.com/huazhechen/cuber/raw/master/screenshot/reverse.gif)

- 自由涂色

  ![colorize](https://gitee.com/huazhechen/cuber/raw/master/screenshot/colorize.gif)

- 节奏控制

  ![movie-pause](https://gitee.com/huazhechen/cuber/raw/master/screenshot/movie-pause.gif)

- 质量配置

  ![movie-quality](https://gitee.com/huazhechen/cuber/raw/master/screenshot/movie-quality.gif)
